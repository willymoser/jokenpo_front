import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';

const endpoint = 'http://localhost:8080/jokenpo/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Assept': 'application/json'
  })
};

export class MatchDTO 
{
  numberRounds: number;
}

@Injectable({
  providedIn: 'root'
})

export class HttpRestService {
  
  constructor(private http: HttpClient) { }

  playJokenpo(numberOfRounds: number): Observable<any> {
    let match = new MatchDTO();
    match.numberRounds = numberOfRounds;
    return this.http.post<any>(endpoint + 'playgame', JSON.stringify(match), httpOptions);
  }

}
