import { Component, Injectable } from '@angular/core';
import { HttpRestService } from './http-rest.service';
import * as $ from 'jquery';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

@Injectable({
  providedIn: 'root'
})

export class AppComponent {

  title = 'Jokenpo';
  results :ResultMatchDTO[];
  numberOfRounds: number = 100;

  constructor(private httpRestService: HttpRestService) { }

  playJokenpo () {
    this.httpRestService.playJokenpo(this.numberOfRounds).subscribe(
      (res) => {
        this.results = <ResultMatchDTO[]> res;
        this.openResultMatchModal();
      },
      (error) => {
        console.log(error);
      }
    )
  }

  private openResultMatchModal() {
    $('#mymodal').modal('show');
  }

  closeResultMatchModal() {
    $('#mymodal').modal('hide');
  }

}

export class ResultMatchDTO 
{
  playerName: string = '';
  victories: number = 0;
  defeats: number = 0;
  empates:  number = 0;
}
